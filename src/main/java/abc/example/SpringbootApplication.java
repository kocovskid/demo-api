package abc.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootApplication {
    public static void main(final String[] pArgs) {
        SpringApplication.run(SpringbootApplication.class, pArgs);
    }

}
