package abc.example.controller;

import abc.example.model.frt.v1.DeviceSubscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class ControllerV1 {
    private static final Logger log = LoggerFactory.getLogger(ControllerV1.class);

    @GetMapping(value = "/ping")
    public ResponseEntity<String> ping() {
        log.info("Start ping()");
        DeviceSubscription ds = new DeviceSubscription();
        return new ResponseEntity<>("Hello", HttpStatus.OK);
    }

}
