<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>3.2.0</version>
        <relativePath/>
    </parent>
    <groupId>abc.example</groupId>
    <artifactId>demo-api</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <name>demo-api</name>
    <description>Demo Open API</description>
    <packaging>jar</packaging>

    <properties>
        <java.version>17</java.version>
        <release.version>17</release.version>

        <!-- Dependencies -->
        <springdoc-openapi-ui.version>1.7.0</springdoc-openapi-ui.version>
        <jackson-databind-nullable.version>0.2.6</jackson-databind-nullable.version>

        <!-- Plugins -->
        <!-- Sonar specific values -->
    </properties>

    <dependencyManagement>
        <dependencies>

        </dependencies>
    </dependencyManagement>

    <dependencies>
        <!-- spring boot -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>
        <!-- Openapi generator / validation -->
        <dependency>
            <groupId>org.openapitools</groupId>
            <artifactId>jackson-databind-nullable</artifactId>
            <version>${jackson-databind-nullable.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springdoc</groupId>
            <artifactId>springdoc-openapi-ui</artifactId>
            <version>${springdoc-openapi-ui.version}</version>
        </dependency>
        <!-- utils -->
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <optional>true</optional>
        </dependency>

        <!-- test dependencies -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>build-helper-maven-plugin</artifactId>
                    <version>3.6.0</version>
                </plugin>
            </plugins>
        </pluginManagement>
        <plugins>
            <plugin>
                <groupId>org.openapitools</groupId>
                <artifactId>openapi-generator-maven-plugin</artifactId>
                <version>7.6.0</version>
                <executions>
                    <execution>
                        <id>generate-api-v1</id>
                        <phase>generate-sources</phase>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                        <configuration>
                            <inputSpec>${project.basedir}/openapi/frt/v1/flight-readiness-api-v1.yml</inputSpec>
                            <generatorName>spring</generatorName>
                            <modelPackage>abc.example.model.frt.v1</modelPackage>
                            <generateApis>false</generateApis>
                            <skipOverwrite>false</skipOverwrite>
                            <generateSupportingFiles>false</generateSupportingFiles>
                            <generateApiDocumentation>false</generateApiDocumentation>
                            <skipIfSpecIsUnchanged>true</skipIfSpecIsUnchanged>
                            <addCompileSourceRoot>true</addCompileSourceRoot>
                            <configOptions>
                                <sourceFolder>.</sourceFolder>
                                <openApiNullable>false</openApiNullable>
                                <dateLibrary>java8</dateLibrary>
                                <useBeanValidation>true</useBeanValidation>
                                <useSpringBoot3>true</useSpringBoot3>
                                <useJakartaEe>true</useJakartaEe>
                                <hideGenerationTimestamp>true</hideGenerationTimestamp>
                            </configOptions>
                        </configuration>
                    </execution>
                    <execution>
                        <id>generate-api-v2</id>
                        <phase>generate-sources</phase>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                        <configuration>
                            <inputSpec>${project.basedir}/openapi/frt/v2/flight-readiness-api-v2.yml</inputSpec>
                            <generatorName>spring</generatorName>
                            <modelPackage>abc.example.model.frt.v2</modelPackage>
                            <generateApis>false</generateApis>
                            <skipOverwrite>false</skipOverwrite>
                            <generateSupportingFiles>false</generateSupportingFiles>
                            <generateApiDocumentation>false</generateApiDocumentation>
                            <skipIfSpecIsUnchanged>true</skipIfSpecIsUnchanged>
                            <addCompileSourceRoot>true</addCompileSourceRoot>
                            <configOptions>
                                <sourceFolder>.</sourceFolder>
                                <openApiNullable>false</openApiNullable>
                                <dateLibrary>java8</dateLibrary>
                                <useBeanValidation>true</useBeanValidation>
                                <useSpringBoot3>true</useSpringBoot3>
                                <useJakartaEe>true</useJakartaEe>
                                <hideGenerationTimestamp>true</hideGenerationTimestamp>
                            </configOptions>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>build-helper-maven-plugin</artifactId>
                <executions>
                    <execution>
                        <id>attach-artifacts</id>
                        <phase>package</phase>
                        <goals>
                            <goal>attach-artifact</goal>
                        </goals>
                        <configuration>
                            <artifacts>
                                <artifact>
                                    <file>${project.basedir}/openapi/frt/v1/flight-readiness-api-v1.yml</file>
                                    <type>yml</type>
                                    <classifier>flight-readiness-api-v1</classifier>
                                </artifact>
                                <artifact>
                                    <file>${project.basedir}/openapi/frt/v2/flight-readiness-api-v2.yml</file>
                                    <type>yml</type>
                                    <classifier>flight-readiness-api-v2</classifier>
                                </artifact>
                                <artifact>
                                    <file>${project.basedir}/openapi/frt/v2/flight-readiness-schema-v2.json</file>
                                    <type>json</type>
                                    <classifier>flight-readiness-schema-v2</classifier>
                                </artifact>
                            </artifacts>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
       </plugins>
    </build>
</project>
