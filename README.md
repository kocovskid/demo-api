# demo-api

## Purpose
### OpenApi + Json schema
Demonstrates two ways to represent openapi schemas,
- Using standard openapi definitions for the request/response objects
- Using $ref include json schema for the request/response objects

Using the includes makes it possible to create a "data dictionary" and definitions of common objects
that can be included into openapi specs.

The same concept can be used against json schemas as they can also import other schema definitions.

### Openapi Plugin

Demonstrates the use of the openapi maven plugin to generate java pojos
```
<groupId>org.openapitools</groupId>
<artifactId>openapi-generator-maven-plugin</artifactId>
```

### Shared contract dependencies

Artifacts like the openapi.yaml definition can also be published alongside of the maven jar using
```
<groupId>org.codehaus.mojo</groupId>
<artifactId>build-helper-maven-plugin</artifactId>
```
so that other projects can use the openapi.yaml file as a dependency in order to generate client java pojos,
thereby creating a "contract" between the client and api implementation.

